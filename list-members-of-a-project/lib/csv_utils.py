import csv
import os

def write_to_csv(file_name, data):
    fieldnames = ['username', 'name', 'role']

    with open(file_name, mode='w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        writer.writeheader()

        for row in data:
            writer.writerow({
                'username': row.get('username', ''),
                'name': row.get('name', ''),
                'role': row.get('role', ''),
            })

def cleanup_csv(input_file, output_file, access_level_mapping):
    try:
        with open(input_file, "r") as infile:
            reader = csv.DictReader(infile)
            rows = list(reader)

        unique_rows = {}
        for row in rows:
            username = row["username"]

            if username not in unique_rows:
                unique_rows[username] = row
            else:
                current_role = unique_rows[username]["role"]
                new_role = row["role"]

                current_access_level = next(
                    (level for level, role in access_level_mapping.items() if role == current_role), None
                )
                new_access_level = next(
                    (level for level, role in access_level_mapping.items() if role == new_role), None
                )

                if new_access_level > current_access_level:
                    unique_rows[username] = row

        with open(output_file, "w", newline="") as outfile:
            fieldnames = rows[0].keys()
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(unique_rows.values())

        print(f"Cleaned data has been written to {output_file}")

        if os.path.exists(input_file):
            os.remove(input_file)

    except Exception as e:
        print(f"An error occurred while cleaning the CSV: {e}")
