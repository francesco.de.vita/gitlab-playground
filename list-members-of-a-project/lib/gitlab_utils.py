import os
import gitlab

def get_project(namespace, project_path):
    try:
        GITLAB_URL = "https://gitlab.com"
        TOKEN = os.getenv("GITLAB_TOKEN")

        if not TOKEN:
            raise EnvironmentError("GITLAB_TOKEN environment variable is not set.")

        gl = gitlab.Gitlab(GITLAB_URL, private_token=TOKEN)

        full_project_path = f"{namespace}/{project_path}"
        project = gl.projects.get(full_project_path)

        return project
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching project: {e}")
        return None
    except Exception as e:
        print(f"Unexpected error: {e}")
        return None

def get_project_parent(namespace, project_path):
    try:

        parent_group_path = project_path.split('/')[0]

        GITLAB_URL = "https://gitlab.com"
        TOKEN = os.getenv("GITLAB_TOKEN")
        if not TOKEN:
            raise EnvironmentError("GITLAB_TOKEN environment variable is not set.")

        gl = gitlab.Gitlab(GITLAB_URL, private_token=TOKEN)

        full_parent_group_path = f"{namespace}/{parent_group_path}"

        parent_group = gl.groups.get(full_parent_group_path)
        return parent_group

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching parent group '{parent_group_path}' under namespace '{namespace}': {e}")
        return None
    except Exception as e:
        print(f"Unexpected error while fetching parent group: {e}")
        return None

def list_project_members(project,export_data,access_level_mapping):
    try:
        members = project.members.list(all=True)
        if members:
            for member in members:
                role_name = access_level_mapping.get(member.access_level, "Unknown Role")
                print(f"- {member.username} ({member.access_level}): {role_name}")
                export_data.append({
                    'username': member.username,
                    'name': member.name,
                    'role': role_name,
                })
        else:
            print(f"No members directly invited to the project '{project.name}'.")
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching project members: {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")

def list_inherited_members(group,export_data,access_level_mapping):
    try:
        members = group.members.list(all=True)
        if members:
            print(f"\nMembers inherited from the group '{group.full_path}':")
            for member in members:
                role_name = access_level_mapping.get(member.access_level, "Unknown Role")
                print(f"- {member.username} ({member.name}): {role_name}")
                export_data.append({
                    'username': member.username,
                    'name': member.name,
                    'role': role_name,
                })
        else:
            print(f"No members inherited from the group '{group.full_path}'.")
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching group members: {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")

def list_project_groups(project,export_data,access_level_mapping):
    try:
        group_members = project.shared_with_groups
        if group_members:
            print(f"\nGroups directly invited to the project '{project.name}':")
            for group in group_members:
                role_name = access_level_mapping.get(group["group_access_level"], "Unknown Role")
                print(f"- {group['group_name']} (Group ID: {group['group_id']}): {role_name}")
                export_data.append({
                    'username': '',
                    'name': '',
                    'role': '',
                })
        else:
            print(f"No groups directly invited to the project '{project.name}'.")

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching groups shared with the project: {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")

def list_shared_groups_and_members(namespace, export_data, added_rows,access_level_mapping):
    try:
        GITLAB_URL = "https://gitlab.com"
        TOKEN = os.getenv("GITLAB_TOKEN")
        if not TOKEN:
            raise EnvironmentError("GITLAB_TOKEN environment variable is not set.")

        gl = gitlab.Gitlab(GITLAB_URL, private_token=TOKEN)

        namespace_group = gl.groups.get(namespace)
        if not namespace_group:
            print(f"No group found for the namespace '{namespace}'.")
            return

        print(f"\nDirect members of the namespace '{namespace}':")
        direct_members = namespace_group.members.list(all=True)
        for member in direct_members:
            role_name = access_level_mapping.get(member.access_level, "Unknown Role")
            print(f"- {member.username} ({member.name}): {role_name}")
            row = {
                'username': member.username,
                'name': member.name,
                'role': role_name,
            }
            row_tuple = tuple(row.items())
            if row_tuple not in added_rows:
                export_data.append(row)
                added_rows.add(row_tuple)

        shared_groups = namespace_group.shared_with_groups
        if shared_groups:
            print(f"\nGroups invited to the namespace '{namespace}':")
            for shared_group in shared_groups:
                print(f"- {shared_group['group_name']} (Group ID: {shared_group['group_id']}, Access Level: {access_level_mapping.get(shared_group['group_access_level'], 'Unknown')})")

                group_details = gl.groups.get(shared_group['group_id'])
                members = group_details.members.list(all=True)
                if members:
                    print(f"  Members of {shared_group['group_name']}:")
                    for member in members:
                        role_name = access_level_mapping.get(member.access_level, "Unknown Role")
                        print(f"  - {member.username} ({member.name}): {role_name}")
                        row = {
                            'username': member.username,
                            'name': member.name,
                            'role': role_name,
                        }
                        row_tuple = tuple(row.items())
                        if row_tuple not in added_rows:
                            export_data.append(row)
                            added_rows.add(row_tuple)
                else:
                    print(f"  No members found in {shared_group['group_name']}.")
        else:
            print(f"No groups are invited to the namespace '{namespace}'.")

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching groups or members under the namespace '{namespace}': {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")
