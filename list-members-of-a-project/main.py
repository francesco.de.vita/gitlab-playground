import click
import os
from lib.gitlab_utils import get_project, get_project_parent, list_project_members, list_inherited_members, \
    list_project_groups, list_shared_groups_and_members
from lib.csv_utils import write_to_csv, cleanup_csv

export_data = []
added_rows = set()

@click.command()
@click.argument('namespace')
@click.argument('project_path')
def main(namespace, project_path):
    global export_data
    access_level_mapping = {
        10: "Guest",
        20: "Reporter",
        30: "Developer",
        40: "Maintainer",
        50: "Owner"
    }
    project = get_project(namespace, project_path)
    if project:
        print(f"\nProject found: {project.name} (ID: {project.id})")
        list_project_members(project, export_data,access_level_mapping)
        list_project_groups(project, export_data,access_level_mapping)
        list_shared_groups_and_members(namespace, export_data,added_rows,access_level_mapping)
        parent_group = get_project_parent(namespace, project_path)
        if parent_group:
            print(f"\nParent group found: {parent_group.full_path} (ID: {parent_group.id})")
            list_inherited_members(parent_group, export_data,access_level_mapping)
        else:
            print("\nNo parent group found or an error occurred.")

        write_to_csv("output_data.csv", export_data)
        project_name = os.path.basename(project_path)
        output_file = f"{project_name}_members.csv"

        cleanup_csv("output_data.csv",output_file,access_level_mapping)
    else:
        print("\nNo project found or an error occurred.")


if __name__ == "__main__":
    main()
