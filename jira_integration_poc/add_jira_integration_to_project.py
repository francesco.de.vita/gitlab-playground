import os
import click
import gitlab

@click.command()
@click.option('--group-name', prompt='GitLab Group Name', help='Name of the GitLab group')
@click.option('--project-name', prompt='GitLab Project Name', help='Name of the GitLab project')
@click.option('--jira-url', prompt='Jira URL', help='URL of the Jira instance')
@click.option('--jira-user', prompt='Jira User', help='Username for Jira')
@click.option('--jira-ticket-regex', prompt='Jira Ticket Regex', help='Regex pattern for Jira tickets')
def configure_jira_integration(group_name, project_name, jira_url, jira_user, jira_ticket_regex):
    access_token = os.environ.get('GITLAB_TOKEN')
    gitlab_url = 'https://gitlab.com'
    main_group = "moneyfarm-tech/"

    try:
        gl = gitlab.Gitlab(gitlab_url, private_token=access_token)
        gl.auth()

        subgroup_path = f"{main_group}{group_name}"
        project = gl.projects.get(f"{subgroup_path}/{project_name}")

        jira_service_settings = {
            'url': jira_url,
            'api_url': f'{jira_url}/rest/api/2',
            'username': jira_user,
            'issues_enabled': False,
            'merge_requests_events': True,
            'commit_events': True,
            'comment_on_event_enabled': True,
            'jira_issue_transition_id': 1,
            'jira_regex': jira_ticket_regex
        }

        services = project.services.list()
        jira_service = next((service for service in services if service.type == 'jira'), None)
        if not jira_service:
            project.services.create({
                'type': 'JiraService',
                'active': True,
                'properties': jira_service_settings
            })
            print(f"Jira service created for '{project_name}' in subgroup '{group_name}'.")
        else:
            jira_service.edit(**jira_service_settings)
            print(f"Jira service updated for '{project_name}' in subgroup '{group_name}'.")

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Failed to retrieve subgroup/project: {e}")
    except gitlab.exceptions.GitlabHttpError as e:
        print(f"Failed to configure Jira integration: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == '__main__':
    configure_jira_integration()
