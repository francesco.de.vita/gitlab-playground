import unittest
from datetime import datetime
from io import StringIO
import sys
from make_report import main,get_projects,get_months,get_project_deployments

class TestFunctions(unittest.TestCase):

    def test_get_projects(self):
        # Test with a valid namespace path
        projects = get_projects('moneyfarm-tech/microservices')
        self.assertNotEqual(projects, None)
        self.assertTrue(isinstance(projects, list))
        self.assertTrue(len(projects) > 0)

        # Test with an invalid namespace path
        projects = get_projects('invalid/namespace')
        self.assertEqual(projects, [])

    def test_get_project_deployments(self):
        # Test with a project that has deployments
        project = get_projects('moneyfarm-tech/microservices')[0]
        deployments = get_project_deployments(project)
        self.assertNotEqual(deployments, None)
        self.assertTrue(isinstance(deployments, list))

        # Test with a project that does not have any deployments
        project = get_projects('moneyfarm-tech/frontend')[0]
        deployments = get_project_deployments(project)
        self.assertEqual(deployments, [])

    def test_get_months(self):
        # Test with a start date and end date spanning multiple months
        start_date = datetime(2023, 1, 1)
        end_date = datetime(2023, 6, 30)
        months = get_months(start_date, end_date)
        self.assertNotEqual(months, None)
        self.assertTrue(isinstance(months, list))
        self.assertEqual(len(months), 6)

        # Test with a start date and end date being the same
        start_date = datetime(2023, 3, 15)
        end_date = datetime(2023, 3, 15)
        months = get_months(start_date, end_date)
        self.assertNotEqual(months, None)
        self.assertTrue(isinstance(months, list))
        self.assertEqual(len(months), 1)
        self.assertEqual(months[0], '2023-03')

    def test_deployment_count(self):
        start_date = datetime(2023, 1, 1)
        end_date = datetime(2023, 2, 28)

        # Redirect stdout to capture the output
        output = StringIO()
        sys.stdout = output

        # Call the main function with test arguments
        main(['--namespace', 'moneyfarm-tech/frontend', '--start-date', start_date.strftime('%Y-%m-%d'), '--end-date', end_date.strftime('%Y-%m-%d')])

        # Reset stdout
        sys.stdout = sys.__stdout__

        # Get the captured output
        output_str = output.getvalue()

        # Assert the expected output
        expected_output = "Data under namespace 'moneyfarm-tech/frontend' added to the CSV file.\n" \
                          "CSV file 'deployments.csv' generated successfully.\n"
        self.assertEqual(output_str, expected_output)

if __name__ == '__main__':
    unittest.main()



