import csv
import os
import sys
from datetime import datetime, timedelta
import gitlab
import click

# GitLab API configuration
GITLAB_URL = 'https://gitlab.com'
PRIVATE_TOKEN = os.environ.get('GITLAB_TOKEN')

# Create GitLab client
gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)


# Retrieve projects under a namespace (excluding archived projects)
def get_projects(namespace_path):
    namespace = gl.groups.get(namespace_path)
    projects = namespace.projects.list(all=True, archived=False)
    return projects


# Retrieve deployments in the production environment for a specific project
def get_project_deployments(project):
    full_project = gl.projects.get(project.id)
    deployments = full_project.deployments.list(environment='production', all=True)
    return deployments

# Calculate the list of the months
def get_months(start_date, end_date):
    current_date = start_date
    months = []
    while current_date <= end_date:
        month = current_date.strftime('%Y-%m')
        if month not in months:
            months.append(month)
        current_date += timedelta(days=1)
    return months

# Main function
@click.command()
@click.option('--namespace', '-n', multiple=True, required=True, help='Namespace(s) to retrieve projects from')
@click.option('--start-date', '-s', type=click.DateTime(), required=True,
              help='Start date (YYYY-MM-DD) for the deployment count')
@click.option('--end-date', '-e', type=click.DateTime(), required=True,
              help='End date (YYYY-MM-DD) for the deployment count')
def main(namespace, start_date, end_date):
    if start_date >= end_date:
        click.echo("Error: 'end-date' must be greater than 'start-date'.")
        sys.exit(1)

    months = get_months(start_date,end_date)

    with open('deployments.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Namespace', 'Project'] + months)  # Write header row

        for namespace_path in namespace:
            projects = get_projects(namespace_path)
            if projects:
                namespace_name = namespace_path.split('/')[-1]

                for project in projects:
                    project_name = project.name
                    deployments = get_project_deployments(project)

                    # Initialize count dictionary for each month
                    count_dict = {month: 0 for month in months}

                    if deployments:
                        for deployment in deployments:
                            if deployment.status == 'success':
                                created_at = datetime.strptime(deployment.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
                                month = created_at.strftime('%Y-%m')
                                if start_date <= created_at <= end_date and month in count_dict:
                                    count_dict[month] += 1

                    # Write row for the project
                    row = [namespace_name, project_name] + [count_dict[month] for month in months]
                    writer.writerow(row)

                click.echo(f"Data under namespace '{namespace_name}' added to the CSV file.")

            else:
                click.echo(f"No projects found under the specified namespace '{namespace_path}'.")

    click.echo("CSV file 'deployments.csv' generated successfully.")


if __name__ == "__main__":
    main()
