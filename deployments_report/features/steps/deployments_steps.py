from behave import given, when, then
from make_report import main

@given('the namespace "{namespace}"')
def step_given_namespace(context, namespace):
    context.namespaces = [namespace]

@given('the namespaces "{namespace1}" and "{namespace2}"')
def step_given_namespaces(context, namespace1, namespace2):
    context.namespaces = [namespace1, namespace2]

@when('I run the deployment count script')
def step_when_run_script(context):
    # Run the script with the provided namespaces
    for namespace in context.namespaces:
        main(['--namespace', namespace])

@then('the output should contain "{text}"')
def step_then_output_contains(context, text):
    # Check if the provided text is present in the captured output
    assert text in context.output

