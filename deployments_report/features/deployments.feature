Feature: Deployment Count

  Scenario: Count deployments for a specific namespace
    Given the namespace "moneyfarm-tech/microservices"
    When I run the deployment count script
    Then the output should contain "Data under namespace 'moneyfarm-tech/microservices' added to the CSV file"

  Scenario: Count deployments for multiple namespaces
    Given the namespaces "moneyfarm-tech/microservices" and "moneyfarm-tech/frontend"
    When I run the deployment count script
    Then the output should contain "Data under namespace 'moneyfarm-tech/microservices' added to the CSV file"
    And the output should contain "Data under namespace 'moneyfarm-tech/frontend' added to the CSV file"
